import java.util.Map;
import com.google.gson.JsonObject;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;


    public class EnrichmentService {
        private final Map<String, Map<String, String>> map;
        EnrichmentService(Map<String, Map<String, String>> map) {
            this.map = map;
        }

        public void enrich(Message message) {
            JsonObject jsonObject = JsonParser.parseString(message.content).getAsJsonObject();//получение JsonObject
            switch (message.enrichmentType) {
                case MSISDN:
                    addMSISDN(jsonObject);
                    break;
            }
            message.content = jsonObject.toString();
        }
        private void addMSISDN(JsonObject jsonObject) {
            String msisdn = String.valueOf(jsonObject.get("msisdn")).substring(1, 12);
            Map<String, String> info = this.map.get(msisdn);
            JsonElement jsonMap = JsonParser.parseString((new Gson()).toJson(info));
            jsonObject.add("enrichment", jsonMap);
        }
    }

