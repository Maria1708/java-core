import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

public class Application {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        int k = 10;
        for (int i = 0; i < k; i++) {
            testing();
        }
    }
    public static void testing() throws ExecutionException, InterruptedException, TimeoutException {
        Map<String, Map<String, String>> map = createUserMap();
        ArrayList<Message> messages = createMessages();
        ExecutorService executor = Executors.newFixedThreadPool(3);
        ArrayList<Future<Message>> message1 = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            int x = i;
            Future<Message> fm = executor.submit(() -> {
                try {
                    return enrichment(map, messages.get(x));
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            message1.add(fm);
        }
        boolean flag = true;
        String first = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81000000000\",\"enrichment\":{\"firstName\":\"ilon\",\"secondName\":\"Mask\"}}";
        String second = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"82000000000\",\"enrichment\":{\"firstName\":\"James\",\"secondName\":\"Bond\"}}";
        String third = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"83000000000\",\"enrichment\":{\"firstName\":\"Filipp\",\"secondName\":\"Kirkorov\"}}";
        if (!Objects.equals(message1.get(0).get(10, TimeUnit.SECONDS).content, first)) flag = false;
        if (!Objects.equals(message1.get(1).get(10, TimeUnit.SECONDS).content, second)) flag = false;
        if (!Objects.equals(message1.get(2).get(10, TimeUnit.SECONDS).content, third)) flag = false;
        if (flag) {
            System.out.println("testings succeeded");
        } else {
            System.out.println("testings failed");
        }
        executor.shutdown();
    }
    public static ArrayList<Message> createMessages() {
        ArrayList<Message> array = new ArrayList<>();

        Message message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "81000000000"
                }""";
        array.add(message);

        message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "82000000000"
                }""";
        array.add(message);

        message = new Message();
        message.enrichmentType = Message.EnrichmentType.MSISDN;
        message.content = """
                {
                  "action": "button_click",
                  "page": "book_card",
                  "msisdn": "83000000000"
                }""";
        array.add(message);

        return array;
    }
    public static Map<String, Map<String, String>> createUserMap() {
        Map<String, Map<String, String>> map = new ConcurrentHashMap<>();
        Map<String, String> temporary = new ConcurrentHashMap<>();
        temporary.put("firstName", "ilon");
        temporary.put("secondName", "Mask");
        map.put("81000000000", temporary);

        temporary = new ConcurrentHashMap<>();
        temporary.put("firstName", "James");
        temporary.put("secondName", "Bond");
        map.put("82000000000", temporary);

        temporary = new ConcurrentHashMap<>();
        temporary.put("firstName", "Filipp");
        temporary.put("secondName", "Kirkorov");
        map.put("83000000000", temporary);

        return map;
    }
    public static Message enrichment(Map<String, Map<String, String>> map, Message message) throws SQLException {
        EnrichmentService service = new EnrichmentService(map);
        service.enrich(message);
        return message;
    }
}
